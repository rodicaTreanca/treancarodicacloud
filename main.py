from flask import Flask
import requests
import json


# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = Flask(__name__)

@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return 'Merge!'

#numarul de angajati total
@app.route('/employees')
def employees():
    print('Numarul de angajati total:')
    res = requests.get('https://ultra-automata-237814.appspot.com/employees')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

# numarul de angajati per departament
@app.route('/departments/employees')
def departmentsEmployees():
    res = requests.get('https://ultra-automata-237814.appspot.com//departments/employees')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

#lista cu departamentele disponibile
@app.route('/departments')
def departments():
    res = requests.get('https://ultra-automata-237814.appspot.com/departments')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

#numarul de angajati femei
@app.route('/employees/gender/female')
def employeesGenderFemale():
    res = requests.get('https://ultra-automata-237814.appspot.com/employees/gender/female')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

#numarul de angajati barbati
@app.route('/employees/gender/male')
def employeesGenderMale():
    res = requests.get('https://ultra-automata-237814.appspot.com/employees/gender/male')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

#lista cu topul de salarii de 100
@app.route('/salaries/top/100')
def salariesTop():
    res = requests.get('https://ultra-automata-237814.appspot.com/salaries/top/100')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )

# numarul de angajati per fiecare titlu de job disponibil
@app.route('/titles ')
def titles():
    res = requests.get('https://ultra-automata-237814.appspot.com/titles ')
    return app.response_class(
        response = json.dumps(res.json()),
        status = 200,
        mimetype = 'application/json'
    )
if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
